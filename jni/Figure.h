#ifndef _OKGAME_FIGURE_H_
#define _OKGAME_FIGURE_H_

#include "Vector.h"

namespace okgame
{
	class Figure
	{
	public :

		enum FigType { Point = 0, Polygon, Circle };

		Figure( FigType a_type,  const Vector<float>& a_pos, int a_vertsCnt, const float* a_verts );
		virtual ~Figure();

		void 			Draw() 			const;
		Vector<float> 	GetPos() 		const;
		int 			GetVertsCnt() 	const;
		const float* 	GetVerts() 		const;

		void Update( const Vector<float>& a_pos, int a_vertsCnt, const float* a_verts );
		void RotateAtPoint( const Vector<float>& a_pos, float a_angle );
		void Shift( const Vector<float>& a_shift );
		void SetPos( const Vector<float>& a_newPos );

		virtual bool IntersectWithPoint( const Vector<float>& a_pos ) 				const = 0;
		virtual bool IntersectWithPolygon( const Figure* a_figure ) 				const = 0;
		virtual bool IntersectWithCircle( const Vector<float>& a_pos, float a_rad ) const = 0;

	protected :

		FigType			m_type;
		Vector<float>	m_pos;
		int 			m_size;
		int				m_vertsCnt;
		float*			m_verts;
	};

} //namespace okgame

#endif
