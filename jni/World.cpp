#include <list>
#include <set>
#include <GLES/gl.h>
#include <GLES/glext.h>
#include <time.h>

#include "World.h"
#include "Context.h"
#include "TimeService.h"
#include "GraphicService.h"
#include "Log.h"
#include "Geometry.h"
#include "NumberDrawer.h"

namespace okgame
{
	const float World::DEPLOY_TIME_INTERVAL 	= 3.0f;
	const float World::FLICKER_TIME_INTERVAL 	= 0.2f;
	const float World::MENU_CHANGE_INTERVAL 	= 0.2f;
	const float World::MENU_POS_FACTOR			= 10.0f;
	const float World::MENU_DECREASE_FAC		= 1.3f;
	const Vector<float> World::BULLET_INIT_DIR( 0.0f, 1.0f );


	World :: World()
		: m_astCnt( 0 ),
		  m_bulletCnt( 0 ),
		  m_deployTime( 0.0f ),
		  m_flickerTime( 0.0f ),
		  m_visibleMode( false ),
		  m_sessionTime( 0.0f ),
		  m_state( MENU ),
		  m_initialized( false )
	{
		std::set<int> astIds;

		m_resManager = new ResourceManager( MAX_AST_CNT, MAX_BULLET_CNT );
		m_menuCircle = new Circle( Vector<float>( 0.0f, 0.0f ), MENU_DEF_RAD );

		srand( time( 0 ) );
	}

	World :: ~World()
	{
		delete m_resManager;
		delete m_menuCircle;
	}

	void World :: AddAsteroid( const Vector<float>& a_pos, const Vector<float>& a_dir, bool a_fragment )
	{
		GameObject* ast;
		int id;

		if ( ( m_astCnt <= MAX_AST_CNT ) && ( m_resManager->GetMemForAsteroid( id, ast ) ) )
		{
			static_cast<Asteroid*> ( ast )->Construct( a_pos, a_dir, a_fragment );
			m_worldObjects.insert( std::make_pair( id, ast ) );
			++m_astCnt;
		}
	}

	void World :: DeleteAsteroid( int a_id )
	{
		if ( m_astCnt > 0 )
		{
			m_resManager->PutBackAsteroidMem( a_id );
			--m_astCnt;
		}
	}

	void World :: AddBullet()
	{
		GameObject* bullet;
		int id;

		if ( ( m_bulletCnt <= MAX_BULLET_CNT ) && ( m_resManager->GetMemForBullet( id, bullet ) ) )
		{
			static_cast<Bullet*> ( bullet )->Construct( m_shipTop, m_bulletVel );
			m_worldObjects.insert( std::make_pair( id, bullet ) );
			++m_bulletCnt;
		}
	}

	void World :: DeleteBullet( int a_id )
	{
		if ( m_bulletCnt > 0 )
		{
			m_resManager->PutBackBulletMem( a_id );
			--m_bulletCnt;
		}
	}

	void World :: GenerateAsteroids()
	{
		int 		astId;
		Asteroid* 	ast;

		const Vector<float> shipPos = m_ship->GetPos();
		for ( int i = m_astCnt; i < AST_CNT; ++i )
		{
			Vector<float> createPos( m_astCreateRadius, 0.0f );
			createPos.Rotate( Geometry::GetRandomAngle() );
			createPos = createPos + shipPos;

			Vector<float> toPos( m_astAttackRadius, 0.0f );
			toPos.Rotate( Geometry::GetRandomAngle() );
			toPos = toPos + shipPos;

			Vector<float> dir( toPos - createPos );
			dir.ToUnit();

			AddAsteroid( createPos, dir, false );
		}
	}

	void World :: GenerateFragmentedAsteroids( const std::list<Vector<float> >& a_toAdd )
	{
		for ( std::list<Vector<float> >::const_iterator it = a_toAdd.begin(); it != a_toAdd.end(); ++it )
		{
			for ( int i = 0; i < AST_FRAG_CNT; ++i )
			{
				Vector<float> dir( 1.0f, 0.0f );
				dir.Rotate( Geometry::GetRandomAngle() );

				AddAsteroid( *it, dir, true );
			}
		}
	}

	void World :: Initialize()
	{
		if ( m_initialized )
			return;

		m_displaySize = Context::m_graphicsService->GetDisplaySize();

		const float shipStartVel 	= m_displaySize.m_y / SHIP_START_VEL_FACTOR;
		const float shipWidth 		= m_displaySize.m_x / SHIP_WIDTH_FACTOR;
		const float	shipHeight		= m_displaySize.m_y / SHIP_HEIGHT_FACTOR;
		const Vector<float> interfSize( shipWidth / INTERF_FACTOR, shipHeight / INTERF_FACTOR );

		if ( !Asteroid::IsInitialized() )
		{
			const float maxRad = m_displaySize.m_x / AST_MAX_RAD_FACTOR;
			const float minRad = m_displaySize.m_x / AST_MIN_RAD_FACTOR;

			Asteroid::Initialize( minRad, maxRad );
		}

		if ( !NumberDrawer::IsInitialized() )
			NumberDrawer::Initialize( interfSize.m_x, interfSize.m_y );

		//In center
		const Vector<float> shipPos( m_displaySize.m_x / 2.0f, m_displaySize.m_y / 2.0f );
		m_astCreateRadius 	= shipPos.m_x + Asteroid::MaxRadius();
		m_destroyRadius		= m_astCreateRadius +  Asteroid::MaxRadius();
		m_astAttackRadius 	= m_displaySize.m_x / AST_ATTACK_RAD_FACTOR;

		m_ship 		= new Ship( shipPos, shipStartVel, shipWidth, shipHeight );
		m_shipTop	= m_ship->GetTop();
		m_bulletVel = ( m_displaySize.m_y / BULLET_VEL_FACTOR ) * BULLET_INIT_DIR;

		//Interface
		m_lifeIndicPos 		= Vector<float>( interfSize.m_x, m_displaySize.m_y - interfSize.m_y );
		m_lifeIndicInterVec = Vector<float>( interfSize.m_x, 0.0f );
		m_scoreIndicPos		= Vector<float>( m_displaySize.m_x - 2.0f * interfSize.m_x, m_displaySize.m_y - interfSize.m_y );
		Ship::CreateShipFigure( m_lifeIndicFigure, m_lifeIndicPos, interfSize.m_x, interfSize.m_y );

		MenuInitialize();

		m_initialized = true;
	}

	void World :: DeInitialize()
	{
		if ( Asteroid::IsInitialized() )
			Asteroid::DeInitialize();

		if ( NumberDrawer::IsInitialized() )
			NumberDrawer::DeInitilize();

		delete m_ship;
		delete m_lifeIndicFigure;

		m_initialized = false;
	}

	void World :: SessionStart()
	{
		m_state 		= PLAYING;
		m_life			= INIT_LIFE_CNT;
		m_deployTime	= 0.0f;
		m_sessionTime	= 0.0f;

		while ( !m_worldObjects.empty() )
		{
			std::map<int, GameObject*>::iterator it = m_worldObjects.begin();

			switch ( it->second->GetType() )
			{
				case GameObject::BULLET :
					DeleteBullet( it->first );
					break;
				case GameObject::FRAG_ASTEROID 	:
				case GameObject::ASTEROID 		:
					DeleteAsteroid( it->first );
					break;
			}

			m_worldObjects.erase( it );
		}

		GenerateAsteroids();
	}

	void World :: DrawInterface()
	{
		m_lifeIndicFigure->SetPos( m_lifeIndicPos );

		for ( int i = m_life; i >0; --i )
		{
			m_lifeIndicFigure->Draw();
			m_lifeIndicFigure->Shift( m_lifeIndicInterVec );
		}

		NumberDrawer::DrawToLeft( m_scoreIndicPos, uint32_t( m_sessionTime ) );
	}

	void World :: RotateWorld( float a_side )
	{
		static const float stepAngle	= Geometry::PI / 100;
		const Vector<float> shipPos 	= m_ship->GetPos();
		const float rotateAngle 		= stepAngle * a_side;

		for ( std::map<int, GameObject*>::iterator it = m_worldObjects.begin(); it != m_worldObjects.end(); ++it )
			it->second->RotateAtPoint( shipPos, rotateAngle );
	}

	void World :: CheckOutObjects()
	{
		const Vector<float> shipPos = m_ship->GetPos();
		std::map<int, GameObject*>::iterator it = m_worldObjects.begin();

		while ( it != m_worldObjects.end() )
		{
			if ( m_destroyRadius + Geometry::EPS < it->second->GetPos().DistTo( shipPos ) )
			{
				if ( it->second->GetType() == GameObject::BULLET )
					DeleteBullet( it->first );
				else if ( ( it->second->GetType() == GameObject::ASTEROID ) ||
						  ( it->second->GetType() == GameObject::FRAG_ASTEROID ) )
				{
					DeleteAsteroid( it->first );
				}

				m_worldObjects.erase( it++ );
			}
			else
				++it;
		}
	}

	void World :: CollisionDetect()
	{
		std::set<int> toDelete;
		std::list<Vector<float> > toAdd;

		for ( std::map<int, GameObject*>::iterator i = m_worldObjects.begin(); i != m_worldObjects.end(); ++i )
		{
			if ( i->second->GetType() != GameObject::BULLET )
				continue;

			for ( std::map<int, GameObject*>::iterator j = m_worldObjects.begin(); j != m_worldObjects.end(); ++j )
			{
				if ( j->second->GetType() == GameObject::BULLET )
					continue;

				//Little optimization
				if ( i->second->GetPos().DistTo( j->second->GetPos() ) + Geometry::EPS < j->second->GetRad() )
				{
					if ( i->second->CollideWithAsteroid( j->second ) && ( toDelete.count( j->first ) == 0 ) )
					{
						toDelete.insert( i->first );
						toDelete.insert( j->first );
						break;
					}
				}
			}
		}

		for ( std::set<int>::iterator it = toDelete.begin(); it != toDelete.end(); ++it )
		{
			std::map<int, GameObject*>::iterator delIt = m_worldObjects.find( *it );

			if ( delIt == m_worldObjects.end() )
				continue;

			if ( ( delIt->second->GetType() == GameObject::ASTEROID ) ||
			     ( delIt->second->GetType() == GameObject::FRAG_ASTEROID )	)
			{
				DeleteAsteroid( delIt->first );

				if ( delIt->second->GetType() == GameObject::ASTEROID )
					toAdd.push_back( delIt->second->GetPos() );
			}
			else
			{
				DeleteBullet( delIt->first );
			}

			m_worldObjects.erase( delIt );
		}

		GenerateFragmentedAsteroids( toAdd );
	}

	bool World :: CheckShipCoollision()
	{
		for ( std::map<int, GameObject*>::iterator it = m_worldObjects.begin(); it != m_worldObjects.end(); ++it )
		{
			if ( it->second->GetType() == GameObject::BULLET )
				continue;

			//Little optimization
			if ( m_ship->GetPos().DistTo( it->second->GetPos() ) + Geometry::EPS < m_ship->GetRad() + it->second->GetRad() )
			{
				if ( m_ship->CollideWithAsteroid( it->second ) )
				{
					DeleteAsteroid( it->first );
					m_worldObjects.erase( it );
					return true;
				}
			}
		}

		return false;
	}

	void World :: MenuInitialize()
	{
		m_state = MENU;

		Vector<float> shipPos = m_ship->GetPos();

		const int xShift   	= m_displaySize.m_x / MENU_POS_FACTOR;
		const int maxWidth 	= m_displaySize.m_x - 2 * xShift;
		const int yShift    = m_displaySize.m_y / MENU_POS_FACTOR;
		const int maxHeight = m_displaySize.m_y - 2 * yShift;

		m_maxMenuRad = m_displaySize.m_x / MENU_MAX_RAD_FAC;
		m_minMenuRad = m_displaySize.m_x / MENU_MIN_RAD_FAC;

		m_menuCircle->SetPos( Vector<float>( xShift + rand() % maxWidth, yShift + rand() % maxHeight ) );
		m_menuCircle->SetRad( m_maxMenuRad );
		m_menuChangeTimer = 0.0f;
	}

	void World :: MenuDraw()
	{
		m_menuCircle->Draw();
	}

	void World :: MenuUpdate( float a_elapsedTime )
	{
		m_menuChangeTimer += a_elapsedTime;

		if ( m_menuChangeTimer + Geometry::EPS < MENU_CHANGE_INTERVAL )
			return;

		m_menuChangeTimer = 0.0f;
		float currRad = m_menuCircle->GetRad();

		if ( currRad + Geometry::EPS < m_minMenuRad )
			currRad = m_maxMenuRad;
		else
			currRad /= MENU_DECREASE_FAC;

		m_menuCircle->SetRad( currRad );
	}

	void World :: NextState( float a_elapsedTime )
	{
		if ( m_state == MENU )
		{
			MenuUpdate( a_elapsedTime );
			return;
		}

		m_sessionTime += a_elapsedTime;

		m_ship->Update( a_elapsedTime );

		const Vector<float> shift = a_elapsedTime * ( -1 * m_ship->GetVel() );

		for ( std::map<int, GameObject*>::iterator it = m_worldObjects.begin(); it != m_worldObjects.end(); ++it )
 		{
 			it->second->Update( a_elapsedTime );
			it->second->Shift( shift );
 		}

		CheckOutObjects();
		CollisionDetect();

		if ( m_state == DEPLOYING )
		{
			m_flickerTime += a_elapsedTime;
			m_deployTime  += a_elapsedTime;

			if ( DEPLOY_TIME_INTERVAL + Geometry::EPS < m_deployTime )
				m_state = PLAYING;
		}
		else if ( CheckShipCoollision() )
		{
			--m_life;

			m_state 	  = DEPLOYING;
			m_deployTime  = 0.0f;
			m_flickerTime = 0.0f;
			m_visibleMode = true;
		}

 		if ( m_life == 0 )
 			MenuInitialize();
 		else
 			GenerateAsteroids();
	}

	void World :: Draw( float a_elapsedTime )
	{
		glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT );

		if ( m_state == MENU )
		{
			m_menuCircle->Draw();
			return;
		}

		if ( m_state == DEPLOYING )
		{
			if ( FLICKER_TIME_INTERVAL + Geometry::EPS < m_flickerTime  )
			{
				m_flickerTime = 0.0f;
				m_visibleMode = !m_visibleMode;
			}

			if ( m_visibleMode )
				m_ship->Draw();
		}
		else
			m_ship->Draw();

		for ( std::map<int, GameObject*>::iterator it = m_worldObjects.begin(); it != m_worldObjects.end(); ++it )
			it->second->Draw();

		DrawInterface();
	}

	status World :: onActivate()
	{
		Context::m_timeService->Reset();

		if ( Context::m_graphicsService->Start() != RES_OK )
			return RES_ERROR;

		Initialize();

		return RES_OK;
	}

	void World :: onDeactivate()
	{
		Context::m_graphicsService->Stop();
	}

	status World :: onStep()
	{
		const float elapsedTime = Context::m_timeService->Elapsed();

		NextState( elapsedTime );
		Draw( elapsedTime );

		if ( Context::m_graphicsService->SwapBuffers() != RES_OK )
			return RES_ERROR;

		return RES_OK;
	}

	void World :: onStart()
	{
		Log::Info( WorkTag, "onStart" );
	}

	void World::onResume()
	{
		Log::Info( WorkTag, "onResume");
	}

	void World::onPause()
	{
		Log::Info( WorkTag, "onPause");
	}

	void World::onStop()
	{
		Log::Info( WorkTag, "onStop");
	}

	void World::onDestroy()
	{
		DeInitialize();
		Log::Info( WorkTag, "onDestroy");
	}

	void World::onSaveState(void** pData, size_t* pSize)
	{
		Log::Info( WorkTag, "onSaveInstanceState");
	}

	void World::onConfigurationChanged()
	{
		Log::Info( WorkTag, "onConfigurationChanged");
	}

	void World::onLowMemory()
	{
		Log::Info( WorkTag, "onLowMemory");
	}

	void World::onCreateWindow()
	{
		Log::Info( WorkTag, "onCreateWindow");
	}

	void World::onDestroyWindow()
	{
		Log::Info( WorkTag, "onDestroyWindow");
	}

	void World::onGainFocus()
	{
		Log::Info( WorkTag, "onGaInfocus");
	}

	void World::onLostFocus()
	{
		Log::Info( WorkTag, "onLostFocus");
	}

	 bool World :: onTouchEvent(AInputEvent* pEvent)
	 {
		 if ( AMotionEvent_getAction( pEvent ) == AMOTION_EVENT_ACTION_DOWN )
		 {
			float touchX = AMotionEvent_getX( pEvent, 0 );
			float touchY = m_displaySize.m_y - AMotionEvent_getY( pEvent, 0 );

			if ( m_state == MENU )
			{
				if ( m_menuCircle->GetPos().DistTo( Vector<float>( touchX, touchY ) ) + Geometry::EPS < m_maxMenuRad )
				{
					SessionStart();
					return true;
				}
			}
			else
			{
				if ( touchX + Geometry::EPS < m_displaySize.m_x / 2 )
					m_ship->SetStartVelocity();
				else
					AddBullet();
			}
		 }

		 return true;
	 }

	 bool World :: onAccelerometerEvent( ASensorEvent* pEvent )
	 {
		 if ( m_state == MENU )
			 return true;

		 const float GRAVITY  	=  ASENSOR_STANDARD_GRAVITY / 2.0f;
		 const float MIN_VAL  	= -1.0f;
		 const float MAX_VAL 	= 1.0f;
		 const float CENTER		= ( MIN_VAL + MAX_VAL ) / 2.0f;

	  	float val = pEvent->vector.y / GRAVITY;

	  	if ( MAX_VAL + Geometry::EPS < val )
	  		val = MAX_VAL;
		else if ( val + Geometry::EPS < MIN_VAL )
			val = MIN_VAL;

		val -= CENTER;

		RotateWorld( -val );

		return true;
	 }

} //namespace okgame
