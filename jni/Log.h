#ifndef _OKGAME_LOG_H_
#define _OKGAME_LOG_H_

namespace okgame
{
	enum LogTags { WorkTag = 0, InitializeTag, UpdateTag };



	class Log
	{
	public :
		static const char* log_tags[];

		static void Error( LogTags a_tag, const char* pMessage, ... );
		static void Warn(  LogTags a_tag, const char* pMessage, ... );
		static void Info(  LogTags a_tag, const char* pMessage, ... );
		static void Debug( LogTags a_tag, const char* pMessage, ... );
	};
}

#ifndef NDEBUG
	#define LogDebug(tag, ...) okgame::Log::Debug( tag, __VA_ARGS__)
#else
	#define LogDebug(tag, ...)
#endif


#endif
