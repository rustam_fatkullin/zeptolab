#include <GLES/gl.h>
#include <GLES/glext.h>

#include "Ship.h"
#include "World.h"
#include "Polygon.h"

namespace okgame
{


	Ship :: Ship( const Vector<float>& a_pos, float a_startVel, float a_width, float a_height  )
		:	GameObject( GameObject::USER_SHIP, Vector<float>( 0.0f, 0.0f ), a_height / 2.0f ),
		 	m_startVelocity( 0.0f, a_startVel )
	{
		if ( a_height + Geometry::EPS < a_width )
			m_rad = a_width / 2.0f;

		m_top = CreateShipFigure( m_figure, a_pos, a_width, a_height );
	}

	Vector<float> Ship :: CreateShipFigure( Figure*& a_figure, const Vector<float>& a_pos, float a_width, float a_height )
	{
		static float verts[ EL_CNT ];

		float halfWidth = a_width / 2.0f;
		float oneThird  = 1.0f / 3.0f * a_height;

		verts[ 0 ] = a_pos.m_x;
		verts[ 1 ] = a_pos.m_y;

		verts[ 2 ] = a_pos.m_x + halfWidth;
		verts[ 3 ] = a_pos.m_y - oneThird;

		verts[ 4 ] = a_pos.m_x;
		verts[ 5 ] = a_pos.m_y + 2 * oneThird;

		verts[ 6 ] = a_pos.m_x - halfWidth;
		verts[ 7 ] = a_pos.m_y - oneThird;

		a_figure = new Polygon( a_pos, VERTS_CNT, verts );

		return Vector<float>( verts[ 4 ], verts[ 5 ] );
	}

	Ship :: ~Ship()
	{
		delete m_figure;
	}

	void Ship :: Update( float a_elapsedTime )
	{
		m_vel = m_vel - a_elapsedTime * World::RESIST_FAC * m_vel;
	}

	Vector<float> Ship :: GetTop()
	{
		return m_top;
	}

	void Ship :: SetStartVelocity()
	{
		m_vel = m_startVelocity;
	}

	bool Ship :: CollideWithShip( const GameObject* a_obj ) const
	{
		return m_figure->IntersectWithPolygon( a_obj->GetFigure() );
	}

	bool Ship :: CollideWithBullet( const  GameObject* a_obj ) const
	{
		return m_figure->IntersectWithPoint( a_obj->GetPos() );
	}

	bool Ship ::  CollideWithAsteroid( const GameObject* a_obj  ) const
	{
		return m_figure->IntersectWithPolygon( a_obj->GetFigure() );
	}

} // namespace okgame
