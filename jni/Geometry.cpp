#include <math.h>
#include <time.h>
#include <stdlib.h>
#include "Geometry.h"

namespace okgame
{
	const float Geometry::EPS 	= 1e-9;
	const float Geometry::PI	= atan( 1.0f ) * 4.0f;

	void Geometry :: RotateAtPoint( float a_pX, float a_pY, float a_angle, float& a_vX, float& a_vY )
	{
		a_vX -= a_pX;
		a_vY -= a_pY;

		const float x = a_vX;
		const float y = a_vY;
		const float cosVal = cos( a_angle );
		const float sinVal = sin( a_angle );

		a_vX =  cosVal * x + sinVal * y;
		a_vY = -sinVal * x + cosVal * y;

		a_vX += a_pX;
		a_vY += a_pY;
	}

	float Geometry :: Dist( float a_ax, float a_ay, float a_bx, float a_by )
	{
		return sqrt( ( a_ax - a_bx ) * ( a_ax - a_bx ) + ( a_ay - a_by ) * ( a_ay - a_by ) );
	}

	float Geometry :: PseudoScalarMul( float a_aX, float a_aY, float a_bX, float a_bY )
	{
		return a_aX * a_bY - a_aY * a_bX;
	}

	bool Geometry :: PointInSegment( float a_x, float a_y, float a_segStartX, float a_segStartY, float a_segEndX, float a_segEndY )
	{
		const float distToSegStart = Dist( a_x, a_y, a_segStartX, a_segStartY );
		const float distToSegEnd   = Dist( a_x, a_y, a_segEndX, a_segEndY );
		const float segLen		   = Dist( a_segStartX, a_segStartY, a_segEndX, a_segEndY );

		if ( fabs( segLen - ( distToSegStart + distToSegEnd ) ) < EPS )
			return true;

		return false;
	}

	bool Geometry :: InInterval( float a_begin, float a_end, float a_target )
	{
		return ( a_begin + EPS < a_target ) && ( a_target + EPS < a_end );
	}

	bool Geometry :: InSegment( float a_begin, float a_end, float a_target )
	{
		return ( fabs( a_target - a_begin ) < EPS ) || ( fabs( a_target - a_end ) < EPS ) || InInterval( a_begin, a_end, a_target );
	}

	 std::pair<float, float> Geometry :: RayPolygonIntersect(	float a_startX,     float a_startY,
																float a_dirVecX,	float a_dirVecY,
																float a_segStartX,  float a_segStartY,
																float a_segEndX,    float a_segEndY )
	{

		float segVecX	 = a_segEndX - a_segStartX;
		float segVecY	 = a_segEndY - a_segStartY;
		float tmpSegVecX = a_startX - a_segStartX;
		float tmpSegVecY = a_startY - a_segStartY;

		float mul1 = PseudoScalarMul( segVecX, segVecY, a_dirVecX, a_dirVecY );

		if ( fabs( mul1 ) < EPS )
		return std::make_pair( -1.0f, -1.0f ); //������� � ��� �����������

		float mul2 = PseudoScalarMul( tmpSegVecX, tmpSegVecY, a_dirVecX, a_dirVecY  );

		float betta = mul2 / mul1;
		float alpha = PseudoScalarMul( tmpSegVecX, tmpSegVecY, segVecX, segVecY ) / mul1;

		if ( InSegment( 0.0f, 1.0f, betta  ) && ( ( fabs( alpha ) < EPS ) || ( alpha > EPS ) ) )
		return std::make_pair( alpha, betta  );

		return std::make_pair( -1.0f, -1.0f );
	}

	bool Geometry :: InPolygon( float a_x, float a_y, int a_n, float *a_verts )
	{
		static const int RAY_LENGTH = 10;

		for ( int i = 0; i < a_n; i += 2 )
		{
			int nextInd = ( i + 2 ) % a_n;

			if ( PointInSegment( a_x, a_y, a_verts[ i ], a_verts[ i + 1 ], a_verts[ nextInd ], a_verts[ nextInd + 1 ] ) )
				return true;
		}

		bool retry = true;
		int intersCnt = 0;
		while ( retry )
		{
			retry = false;
			float angle = 2 * PI * float( rand() / float( RAND_MAX ));

			float dirVecX = RAY_LENGTH * cos( angle );
			float dirVecY = RAY_LENGTH * sin( angle );

			for ( int i = 0; i < a_n; i += 2 )
			{
				int nextInd = ( i + 2 ) % a_n;

				std::pair<float, float> res = RayPolygonIntersect(	a_x, 				a_y,
																	dirVecX, 			dirVecY,
																	a_verts[ i ], 		a_verts[ i + 1 ],
																	a_verts[ nextInd ], a_verts[ nextInd + 1 ] );

				if ( InSegment( 0.0f, 1.0f, res.second ) )
				{
					if ( !InInterval( 0.0f, 1.0f, res.second ) )
					{
						retry = true;
						break;
					}

					++intersCnt;
				}
			}
		}

		return ( intersCnt % 2 == 1 );
	}

	float Geometry :: GetRandomAngle()
	{
		return 2 * PI * ( float( rand() ) / RAND_MAX );
	}

	bool Geometry :: PolygonsIntersection( int a_aN, const float* a_aVerts, int a_bN, const float* a_bVerts )
	{
		for ( int i = 0; i < a_aN; i += 2 )
		{
			int aNextInd = ( i + 2 ) % a_aN;

			float dirX = a_aVerts[ aNextInd ] 	  - a_aVerts[ i ];
			float dirY = a_aVerts[ aNextInd + 1 ] - a_aVerts[ i + 1 ];

			for ( int j = 0; j < a_bN; j += 2 )
			{
				int bNextInd = ( j + 2 ) % a_bN;

				std::pair<float, float>	res = RayPolygonIntersect(	a_aVerts[ i ], 			a_aVerts[ i + 1 ],
																	dirX, 					dirY,
																	a_bVerts[ j ], 			a_bVerts[ j + 1 ],
																	a_bVerts[ bNextInd ], 	a_bVerts[ bNextInd + 1 ] );

				if ( InSegment( 0.0f, 1.0f, res.first ) && InSegment( 0.0f, 1.0f, res.second ) )
					return true;
			}
		}

		return false;
	}

} // namespace okgame
