
#include "Polygon.h"
#include "Geometry.h"

namespace okgame
{
	Polygon :: Polygon( const Vector<float>& a_pos, int a_vertsCnt, const float* a_verts )
		: Figure( Figure::Polygon, a_pos, a_vertsCnt, a_verts )
	{}

	Polygon :: ~Polygon()
	{}

	bool Polygon :: IntersectWithPoint( const Vector<float>& a_pos ) const
	{
		return Geometry::InPolygon( a_pos.m_x, a_pos.m_y, 2 * m_vertsCnt, m_verts );
	}

	bool Polygon :: IntersectWithPolygon( const Figure* a_figure ) const
	{
		return Geometry::PolygonsIntersection( 2 * m_vertsCnt, m_verts, 2 * a_figure->GetVertsCnt(), a_figure->GetVerts() );
	}

	bool Polygon :: IntersectWithCircle( const Vector<float>& a_pos, float a_rad ) const
	{
		//TO FIX
		return false;
	}

} //namespace okgame
