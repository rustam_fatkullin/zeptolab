#ifndef _OKGAME_POLYGON_H_
#define _OKGAME_POLYGON_H_

#include "Vector.h"
#include "Figure.h"

namespace okgame
{
	class Polygon : public Figure
	{
	public :

		Polygon( const Vector<float>& a_pos, int a_vertsCnt, const float* a_verts );
		virtual ~Polygon();

		virtual bool IntersectWithPoint( const Vector<float>& a_pos ) const;
		virtual bool IntersectWithPolygon( const Figure* a_figure ) const;
		virtual bool IntersectWithCircle( const Vector<float>& a_pos, float a_rad ) const;
	};

} //namespace okgame

#endif
