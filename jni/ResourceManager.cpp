#include "ResourceManager.h"

namespace okgame
{
	ResourceManager :: ResourceManager( int a_maxAstCnt, int a_maxBulletCnt )
	{
		int id = -1;

		for ( int i = 0; i < a_maxAstCnt; ++i )
		{
			id = m_idKeeper.GetUniqueId();
			GameObject* ast = new Asteroid();
			m_asteroidsMem.insert( std::make_pair( id, ast ) );
			m_unusedAstIds.insert( id );
		}

		for ( int i = 0; i < a_maxBulletCnt; ++i )
		{
			id = m_idKeeper.GetUniqueId();
			GameObject* bullet = new Bullet();
			m_bulletsMem.insert( std::make_pair( id, bullet ) );
			m_unusedBulletIds.insert( id );
		}
	}

	ResourceManager :: ~ResourceManager()
	{
		for ( std::map<int, GameObject*>::iterator it = m_asteroidsMem.begin(); it != m_asteroidsMem.end(); ++it )
			delete it->second;

		for ( std::map<int, GameObject*>::iterator it = m_bulletsMem.begin(); it != m_bulletsMem.end(); ++it )
			delete it->second;
	}

	bool ResourceManager :: GetMem( int& a_id, GameObject*& a_mem, std::set<int>& a_unusedIds, std::map<int, GameObject*>& a_memMap )
	{
		std::set<int>::iterator it = a_unusedIds.begin();

		if ( it == a_unusedIds.end() )
		{
			a_mem = NULL;
			a_id  = -1;
			return false;
		}

		const int id = *it;
		a_unusedIds.erase( it );

		a_mem = a_memMap[ id ];
		a_id  = id;

		return true;
	}

	void ResourceManager :: PutMem( int a_id, std::set<int>& a_unusedIds, std::map<int, GameObject*>& a_memMap  )
	{
		std::map<int, GameObject*>::iterator it = a_memMap.find( a_id );

		if ( a_memMap.count( a_id ) )
			a_unusedIds.insert( a_id );
	}

	bool ResourceManager :: GetMemForAsteroid( int& a_id, GameObject*& a_newAst )
	{
		return GetMem( a_id, a_newAst, m_unusedAstIds, m_asteroidsMem );
	}

	bool ResourceManager :: GetMemForBullet( int& a_id, GameObject*& a_newBullet )
	{
		return GetMem( a_id, a_newBullet, m_unusedBulletIds, m_bulletsMem );
	}

	void ResourceManager :: PutBackAsteroidMem( int a_id )
	{
		 PutMem( a_id, m_unusedAstIds, m_asteroidsMem );
	}

	void ResourceManager :: PutBackBulletMem( int a_id )
	{
		PutMem( a_id, m_unusedBulletIds, m_bulletsMem );
	}

} // namespace okgame
