#include "GameObject.h"

namespace okgame
{
	GameObject :: GameObject( ObjectType a_type, const Vector<float>& a_vel, float a_rad )
		: m_type( a_type ),
		  m_vel( a_vel ),
		  m_rad( a_rad ),
		  m_figure( NULL )
	{}

	GameObject :: ~GameObject()
	{}

	GameObject::ObjectType GameObject :: GetType()
	{
		return m_type;
	}

	void GameObject :: Draw()
	{
		m_figure->Draw();
	}

	Vector<float> GameObject :: GetPos() const
	{
		return m_figure->GetPos();
	}

	Vector<float> GameObject :: GetVel() const
	{
		return m_vel;
	}

	float GameObject :: GetRad() const
	{
		return m_rad;
	}

	void GameObject :: Update( float a_elapsedTime )
	{
		Shift( a_elapsedTime * m_vel );
	}

	void GameObject :: Shift( const Vector<float>& a_shift )
	{
		m_figure->Shift( a_shift );
	}

	void GameObject :: RotateAtPoint( const Vector<float>& a_pos, float a_angle )
	{
		m_vel.Rotate( a_angle );
		m_figure->RotateAtPoint( a_pos, a_angle );
	}

	const Figure* GameObject :: GetFigure() const
	{
		return m_figure;
	}

} //namespace okgame

