#include <GLES/gl.h>
#include <GLES/glext.h>

#include "Circle.h"
#include "Geometry.h"

namespace okgame
{
	Circle :: Circle( const Vector<float>& a_pos, float a_rad )
		: Figure( Figure::Circle, a_pos, 0, NULL ),
		  m_rad( a_rad )
	{
		m_vertsCnt 	= VERTS_CNT;
		m_verts 	= new float [ 2 * VERTS_CNT ];

		ConstructPoints();
	}

	Circle :: ~Circle()
	{
	}

	void Circle :: ConstructPoints()
	{
		const float angleStep = Geometry::PI / m_vertsCnt;
		float angle 		  = 0.0;

		for ( int i = 0; i < 2 * m_vertsCnt; i += 2 )
		{
			angle = i * angleStep;
			m_verts[ i ]     = m_pos.m_x + m_rad * cos( angle );
			m_verts[ i + 1 ] = m_pos.m_y + m_rad * sin( angle );
		}
	}

	float Circle :: GetRad()
	{
		return m_rad;
	}

	void Circle :: SetRad( float a_rad )
	{
		m_rad = a_rad;

		ConstructPoints();
	}

	bool Circle :: IntersectWithPoint( const Vector<float>& a_pos ) const
	{
		return m_pos.DistTo( a_pos ) < Geometry::EPS;
	}

	bool Circle :: IntersectWithPolygon( const Figure* a_figure ) const
	{
		//:)
		return false;
	}

	bool Circle :: IntersectWithCircle( const Vector<float>& a_pos, float a_rad ) const
	{
		//:)
		return false;
	}


} //namespace okgame

