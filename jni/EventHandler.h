#ifndef _OKGAME_EVENTHANDLER_H_
#define _OKGAME_EVENTHANDLER_H_

#include <android_native_app_glue.h>
#include <android/sensor.h>

#include "Types.h"

namespace okgame
{
	class EventHandler
	{
	public :
		virtual status onActivate()   = 0;
		virtual void   onDeactivate() = 0;
		virtual status onStep()       = 0;

		virtual void onStart()   {};
		virtual void onResume()  {};
		virtual void onPause()   {};
		virtual void onStop()    {};
		virtual void onDestroy() {};

		virtual void onSaveState( void** pData, size_t* pSize ) {};
		virtual void onConfigurationChanged() 					{};
		virtual void onLowMemory() 								{};

		virtual void onCreateWindow()  	   {};
		virtual void onDestroyWindow() 	   {};
		virtual void onGainFocus()         {};
		virtual void onLostFocus()         {};

		//Input events
		virtual bool onTouchEvent( AInputEvent* pEvent) = 0;

		//Accelerometer events
		virtual bool onAccelerometerEvent( ASensorEvent* pEvent ) = 0;
	};
}

#endif
