#include "IdKeeper.h"

namespace okgame
{
	IdKeeper :: IdKeeper()
		: m_currId( 0 )
	{}

	int IdKeeper :: GetUniqueId()
	{
		while ( m_uniqueIds.count( m_currId ) > 0 )
			++m_currId;

		m_uniqueIds.insert( m_currId );

		return m_currId++;
	}

	void IdKeeper :: DestroyUniqueId( int a_id )
	{
		m_uniqueIds.erase( a_id );
	}

} // namespace okgame
