#ifndef _OKGAME_ASTEROID_H_
#define _OKGAME_ASTEROID_H_

#include <vector>

#include "Vector.h"
#include "Polygon.h"
#include "GameObject.h"

namespace okgame
{
	class Asteroid : public GameObject
	{
	public :

		Asteroid();
		Asteroid( const Vector<float>& a_pos, const Vector<float>& a_dir, bool a_frag );
		~Asteroid();

		static bool IsInitialized();
		static void Initialize( int a_fragRad, int a_normRad );
		static void DeInitialize();

		void Construct( const Vector<float>& a_pos, const Vector<float>& a_dir, bool a_frag );
		bool IsFrag();

		static float MaxRadius();

		virtual bool CollideWithShip( const GameObject* a_obj ) const;
		virtual bool CollideWithBullet( const GameObject* a_obj ) const;
		virtual bool CollideWithAsteroid( const GameObject* a_obj ) const;

	private :

		static const int PATTERNS_CNT 		= 10;
		static const int MIN_AST_VERT_CNT	= 4;
		static const int VAR_AST_VERT_CNT	= 30;
		static const int MAX_AST_VERT_CNT	= MIN_AST_VERT_CNT + VAR_AST_VERT_CNT - 1;
		static const int GAP_WIDTH			= 20;
		static const int ROTATE_VARIANTS    = 4;
		static const int MIN_VELOCITY		= 20;
		static const int MAX_VELOCITY		= 50;

		static float fragmentRadius;
		static float normRadius;

		struct AsteroidPattern
		{
			int 	m_vertsCnt;
			float 	m_verts[ 2 * MAX_AST_VERT_CNT ];
		};

		static bool			   m_initialized;
		static AsteroidPattern m_bigAstPatterns[ PATTERNS_CNT ];
		static AsteroidPattern m_smallAstPatterns[ PATTERNS_CNT ];

		static void GeneratePattern( int a_rad, int& vertsCnt, float a_vert[] );

		bool m_frag;
	};

} //namespace okgame

#endif
