#include <GLES/gl.h>
#include <GLES/glext.h>

#include "Context.h"
#include "GraphicService.h"
#include "Log.h"

namespace okgame
{
	GraphicsService :: GraphicsService()
	{
		m_width		  = 0;
		m_height 	  = 0;
		m_display	  = EGL_NO_DISPLAY;
		m_surface	  = EGL_NO_SURFACE;
		m_context	  = EGL_NO_CONTEXT;
	}

	Vector<float> GraphicsService :: GetDisplaySize()
	{
		return Vector<float>( m_width, m_height );
	}

	GraphicsService :: ~GraphicsService()
	{
		//���������
	}

	status GraphicsService :: Start()
	{
		EGLint 		 format;
		EGLint 		 numConfigs;
		EGLint 		 errorResult;
		EGLConfig 	 config;
		const EGLint attributes[] = { EGL_RENDERABLE_TYPE, EGL_OPENGL_ES_BIT, EGL_BLUE_SIZE, 5, EGL_GREEN_SIZE, 6, EGL_RED_SIZE, 5,
									  EGL_SURFACE_TYPE, EGL_WINDOW_BIT, EGL_NONE };

		do
		{
			if ( ( m_display = eglGetDisplay( EGL_DEFAULT_DISPLAY ) ) == EGL_NO_DISPLAY )
			{
				Log::Error( InitializeTag, "eglGetDisplay failed." );
				break;
			}
			Log::Info( InitializeTag, "eglGetDisplay success." );


			if ( !eglInitialize( m_display, NULL, NULL ) )
			{
				Log::Error( InitializeTag, "eglInitialize failed." );
				break;
			}
			Log::Info( InitializeTag, "eglInitialize success." );

			if ( ( !eglChooseConfig( m_display, attributes, &config, 1, &numConfigs ) ) || ( numConfigs <= 0 ) )
			{
				Log::Error( InitializeTag, "eglChooseConfig result - %x. Config numbers - %d", eglGetError(), numConfigs );
				break;
			}
			Log::Info( InitializeTag, "eglChooseConfig success." );

			if ( !eglGetConfigAttrib( m_display, config, EGL_NATIVE_VISUAL_ID, &format ) )
			{
				Log::Error( InitializeTag, "eglGetConfigAttrib failed." );
				break;
			}
			Log::Info( InitializeTag, "eglGetConfigAttrib success." );


			if ( Context::m_app->window == NULL )
				Log::Info( InitializeTag, "Is NULL pointer." );

			if ( ANativeWindow_setBuffersGeometry( Context::m_app->window, 0, 0, format ) )
			{
				Log::Error( InitializeTag, "ANativeWindow_setBuffersGeometry failed." );
				break;
			}
			Log::Info( InitializeTag, "ANativeWindow_setBuffersGeometry success." );

			if ( ( m_surface = eglCreateWindowSurface( m_display, config, Context::m_app->window, NULL ) ) == EGL_NO_SURFACE )
			{
				Log::Error( InitializeTag, "eglCreateWindowSurface failed." );
				break;
			}
			Log::Info( InitializeTag, "eglCreateWindowSurface success." );

			if ( ( m_context = eglCreateContext( m_display, config, EGL_NO_CONTEXT, NULL ) ) == EGL_NO_CONTEXT )
			{
				Log::Error( InitializeTag, "eglCreateContext failed." );
				break;
			}
			Log::Info( InitializeTag, "eglCreateContext success." );

			if ( !eglMakeCurrent( m_display, m_surface, m_surface, m_context ) )
			{
				Log::Error( InitializeTag, "eglMakeCurrent failed." );
				break;
			}
			Log::Info( InitializeTag, "eglMakeCurrent success." );

			if ( !eglQuerySurface( m_display, m_surface, EGL_WIDTH, &m_width )  ||
			     !eglQuerySurface( m_display, m_surface, EGL_HEIGHT, &m_height ) ||
			     ( m_width <= 0 ) || ( m_height <= 0 ) )
			{

				Log::Error( InitializeTag, "eglQuerySurface failed." );
				break;
			}
			Log::Info( InitializeTag, "eglQuerySurface success. Display parameters: %dx%d", m_width, m_height );

			Setup();

			Log::Info( InitializeTag, "OpenGL  is initialized!" );

			return RES_OK;
		} while ( 0 );

		Log::Error( InitializeTag, "Error while starting GraphicsService" );
		Stop();
		return RES_ERROR;
	}

	void GraphicsService ::	Setup()
	{
//		glDisable( GL_DITHER );
//		glDisable( GL_DEPTH_TEST );
//		glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST );

		glEnable( GL_TEXTURE_2D );
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable( GL_BLEND);

		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();
		glOrthof( 0, m_width, 0, m_height, -1.0f, 1.0f );
		glViewport( 0, 0, m_width, m_height );
	}

	void GraphicsService :: Stop()
	{

		if ( m_display != EGL_NO_DISPLAY )
		{
			eglMakeCurrent( m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT );

			if ( m_context != EGL_NO_CONTEXT )
			{
				eglDestroyContext( m_display, m_context );
				m_context = EGL_NO_CONTEXT;
			}

			if ( m_surface != EGL_NO_SURFACE )
			{
				eglDestroySurface( m_display, m_surface );
				m_surface = EGL_NO_SURFACE;
			}

			eglTerminate( m_display );
			m_display = EGL_NO_DISPLAY;
		}
	}

	status GraphicsService :: SwapBuffers()
	{
		if (eglSwapBuffers( m_display, m_surface) != EGL_TRUE)
		{
			Log::Error( WorkTag, "Error %d swapping buffers.", eglGetError() );
			return RES_ERROR;
		}

		return RES_OK;
	}

} // namespace okgame
