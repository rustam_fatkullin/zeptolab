#include <algorithm>
#include <utility>
#include <GLES/gl.h>
#include <GLES/glext.h>
#include <time.h>

#include "Geometry.h"
#include "Asteroid.h"
#include "Log.h"

namespace okgame
{
	Asteroid::AsteroidPattern Asteroid :: m_bigAstPatterns[ PATTERNS_CNT ];
	Asteroid::AsteroidPattern Asteroid :: m_smallAstPatterns[ PATTERNS_CNT ];
	bool Asteroid::m_initialized   = false;
	float Asteroid::fragmentRadius = Asteroid::GAP_WIDTH;
	float Asteroid::normRadius	   = Asteroid::GAP_WIDTH;

	Asteroid :: Asteroid()
		: GameObject( GameObject::ASTEROID, Vector<float>( 0.0f, 0.0f ), GAP_WIDTH )
	{
		m_figure = new Polygon( Vector<float>( 0.0f, 0.0f ), MAX_AST_VERT_CNT, m_smallAstPatterns[ 0 ].m_verts );
	}

	Asteroid :: Asteroid( const Vector<float>& a_pos, const Vector<float>& a_dir, bool a_frag )
		:	GameObject( GameObject::ASTEROID, Vector<float>( 0.0f, 0.0f ), GAP_WIDTH )
	{
		m_figure = new Polygon( Vector<float>( 0.0f, 0.0f ), MAX_AST_VERT_CNT, m_smallAstPatterns[ 0 ].m_verts );
		Construct( a_pos, a_dir, a_frag );
	}

	Asteroid :: ~Asteroid()
	{
		delete m_figure;
		Log::Info( WorkTag, "Asteroid destructor." );
	}

	void Asteroid :: Construct( const Vector<float>& a_pos, const Vector<float>& a_dir, bool a_frag )
	{
		m_type = ( a_frag ? GameObject::FRAG_ASTEROID : GameObject::ASTEROID );
		m_frag = a_frag;

		const int patternInd = rand() % PATTERNS_CNT;

		if ( a_frag )
		{
			m_figure->Update( a_pos, m_smallAstPatterns[ patternInd ].m_vertsCnt, m_smallAstPatterns[ patternInd ].m_verts );
			m_rad = fragmentRadius;
		}
		else
		{
			m_figure->Update( a_pos, m_bigAstPatterns[ patternInd ].m_vertsCnt, m_bigAstPatterns[ patternInd ].m_verts );
			m_rad = normRadius;
		}

		m_vel = ( MIN_VELOCITY + rand() % MAX_VELOCITY ) * a_dir;
	}

	bool Asteroid :: IsFrag()
	{
		return m_frag;
	}

	float Asteroid :: MaxRadius()
	{
		return normRadius;
	}

	struct PolarPoint
	{
		float m_angle;
		float m_length;
		int	  m_ind;
	};

	bool Cmp( const PolarPoint& a_a, const PolarPoint& a_b )
	{
		if ( a_a.m_angle + Geometry::EPS < a_b.m_angle )
			return true;
		else if ( a_b.m_angle + Geometry::EPS < a_a.m_angle )
			return false;
		else
			return a_a.m_length + Geometry::EPS < a_b.m_length;
	}

	void Asteroid :: GeneratePattern( int a_rad, int& vertsCnt, float a_vert[] )
	{
		const Vector<float> center( 0.0f, 0.0f );
		PolarPoint* polarVerts	= new PolarPoint [ MAX_AST_VERT_CNT ];
		Vector<float>* verts	= new Vector<float> [ MAX_AST_VERT_CNT ];
		vertsCnt				= MIN_AST_VERT_CNT + rand() % VAR_AST_VERT_CNT;

		verts[ 0 ] = Vector<float>(  a_rad + rand() % GAP_WIDTH, -a_rad - rand() % GAP_WIDTH );
		verts[ 1 ] = Vector<float>(  a_rad + rand() % GAP_WIDTH,  a_rad + rand() % GAP_WIDTH );
		verts[ 2 ] = Vector<float>( -a_rad - rand() % GAP_WIDTH,  a_rad + rand() % GAP_WIDTH );
		verts[ 3 ] = Vector<float>( -a_rad - rand() % GAP_WIDTH, -a_rad - rand() % GAP_WIDTH );

		for ( int i = MIN_AST_VERT_CNT; i < vertsCnt; ++i )
		{
			verts[ i ] = Vector<float>( a_rad + rand() % GAP_WIDTH, -a_rad + rand() % ( 2 * a_rad ) );

			const float rotateAngle = rand() % ROTATE_VARIANTS * Geometry::PI / 2.0f;

			verts[ i ].Rotate( rotateAngle );
		}

		for ( int i = 0; i < vertsCnt; ++i )
		{
			polarVerts[ i ].m_angle  = atan2( verts[ i ].m_y - center.m_y, verts[ i ].m_x - center.m_x );
			polarVerts[ i ].m_length = center.DistTo( verts[ i ] );
			polarVerts[ i ].m_ind    = i;
		}

		std::sort ( polarVerts, polarVerts + vertsCnt, Cmp );

		float* fVerts = new float [ 2 * MAX_AST_VERT_CNT ];

		for ( int i = 0; i < vertsCnt; ++i )
		{
			a_vert[ 2 * i + 1 ] = verts[ polarVerts[ i ].m_ind ].m_y;
			a_vert[ 2 * i ]     = verts[ polarVerts[ i ].m_ind ].m_x;
		}

		delete [] polarVerts;
		delete [] verts;
	}

	bool Asteroid :: IsInitialized()
	{
		return m_initialized;
	}

	void Asteroid :: Initialize( int a_fragRad, int a_normRad )
	{
		if ( m_initialized )
			return;

		for ( int i = 0; i < PATTERNS_CNT; ++i )
			GeneratePattern( a_fragRad, m_smallAstPatterns[ i ].m_vertsCnt, m_smallAstPatterns[ i ].m_verts );

		for ( int i = 0; i < PATTERNS_CNT; ++i )
			GeneratePattern( a_normRad, m_bigAstPatterns[ i ].m_vertsCnt, m_bigAstPatterns[ i ].m_verts );

		fragmentRadius = a_fragRad + GAP_WIDTH;
		normRadius	   = a_normRad + GAP_WIDTH;

		m_initialized = true;
	}

	void Asteroid :: DeInitialize()
	{
		if ( !m_initialized )
			return;

		m_initialized = false;
	}

	bool Asteroid :: CollideWithShip( const GameObject* a_obj ) const
	{
		return m_figure->IntersectWithPolygon( a_obj->GetFigure() );
	}

	bool Asteroid :: CollideWithBullet( const GameObject* a_obj ) const
	{
		return m_figure->IntersectWithPoint( a_obj->GetPos() );
	}

	bool Asteroid :: CollideWithAsteroid( const GameObject* a_obj ) const
	{
		return m_figure->IntersectWithPolygon( a_obj->GetFigure() );
	}

} // namespace okgame
