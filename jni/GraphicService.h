#ifndef _OKGAME_GRAPHICS_SERVICE_HPP_
#define _OKGAME_GRAPHICS_SERVICE_HPP_

#include <android_native_app_glue.h>
#include <EGL/egl.h>

#include "Types.h"
#include "Vector.h"

namespace okgame
{
	class GraphicsService
	{
	public :
		GraphicsService();
		~GraphicsService();

		status 			Start();
		void			Stop();
		status 			SwapBuffers();
		Vector<float>	GetDisplaySize();

	private :

		void Setup();

		int32_t		 	m_width;
		int32_t		 	m_height;

		EGLDisplay	 	m_display;
		EGLSurface	 	m_surface;
		EGLContext	 	m_context;


	};

} //namespace okgame



#endif
