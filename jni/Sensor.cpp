#include "Sensor.h"
#include "Log.h"
#include "EventLoop.h"

namespace okgame
{
	Sensor :: Sensor( EventLoop& pEventLoop, int32_t pSensorType )
		: mEventLoop( pEventLoop ),
		  mSensor( NULL ),
		  mSensorType( pSensorType )
	{}

	status Sensor :: toggle()
	{
		return ( mSensor != NULL ) ? disable() : enable();
	}

	status Sensor::enable()
	{
		status res = RES_OK;

		do
		{
			if ( mEventLoop.mEnabled )
			{
				mSensor = ASensorManager_getDefaultSensor( mEventLoop.mSensorManager, mSensorType );

				if ( mSensor != NULL )
				{
					if ( ASensorEventQueue_enableSensor( mEventLoop.mSensorEventQueue, mSensor ) < 0 )
					{
						res = RES_ERROR;
						break;
					}

					const int32_t lMinDelay = ASensor_getMinDelay( mSensor );

					if ( ASensorEventQueue_setEventRate( mEventLoop.mSensorEventQueue, mSensor, lMinDelay ) < 0 )
					{
						res = RES_ERROR;
						break;
					}
				}
				else
					Log::Error( WorkTag, "No sensor type %d.", mSensorType );
			}
			else
				Log::Error( WorkTag, "Event loop isn't enable" );
		} while ( 0 );

		if ( res == RES_ERROR )
		{
			Log::Error( WorkTag, "Error while activating sensor." );
			disable();
		}
		else
			Log::Info( WorkTag, "Sensor successfully activated." );

		return res;
	}

	status Sensor :: disable()
	{
		status res = RES_OK;

		if ( ( mEventLoop.mEnabled ) && ( mSensor != NULL ) )
		{
			if ( ASensorEventQueue_disableSensor( mEventLoop.mSensorEventQueue, mSensor ) < 0 )
			{
				res = RES_ERROR;
				Log::Error( WorkTag, "Error while deactivating sensor." );
			}
			else
				mSensor = NULL;
		}

		return res;
	}

} // namespace okgame
