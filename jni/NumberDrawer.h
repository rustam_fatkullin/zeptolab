#ifndef _OKGAME_NUMBER_DRAWER_H_
#define _OKGAME_NUMBER_DRAWER_H_

#include <stdint.h>
#include "Vector.h"

namespace okgame
{
	class NumberDrawer
	{
	public :

		static bool IsInitialized();

		static void Initialize( float a_width, float a_height );
		static void DeInitilize();

		static void DrawToRight( const Vector<float>& a_pos, uint32_t a_num );
		static void DrawToLeft( const Vector<float>& a_pos, uint32_t a_num );

	private :

		static void DrawDigit( int a_digit, float a_x, float a_y );

		static const int MAX_UINT32_LEN			= 10;
		static const int DIGIT_CNT 				= 10;
		static const int MAX_VERTS_CNT 			= 10;
		static const int DIGIT_INTERV_FACTOR 	= 5;
		static const int MAX_EL_CNT 			= 2 * MAX_VERTS_CNT;
		static const int VERTS_CNT[ DIGIT_CNT ];
		static const float DIGIT_VERTS[ DIGIT_CNT ][ MAX_EL_CNT ];

		static float* m_digitVerts[ DIGIT_CNT ];
		static float  m_drawer[ MAX_EL_CNT ];
		static float  m_digitInterval;
		static float  m_width;
		static float  m_height;
		static bool   m_initialized;
	};

} //namespace okgame

#endif
