#ifndef _OKGAME_ID_KEEPER_H_
#define _OKGAME_ID_KEEPER_H_

#include <set>

namespace okgame
{
	class IdKeeper
	{
	public :

		IdKeeper();

		int GetUniqueId();
		void DestroyUniqueId( int a_id );

	private :

		int				m_currId;
		std::set<int>	m_uniqueIds;
	};

} //namespace okgame

#endif
