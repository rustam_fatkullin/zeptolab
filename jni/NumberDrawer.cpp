#include <GLES/gl.h>
#include <GLES/glext.h>
#include <stdlib.h>

#include "NumberDrawer.h"

namespace okgame
{
	const int NumberDrawer::VERTS_CNT[ DIGIT_CNT ] = { 8, 2, 10, 8, 6, 10, 10, 4, 10, 10 };
	float* NumberDrawer::m_digitVerts[ DIGIT_CNT ];
	float NumberDrawer::m_drawer[ MAX_EL_CNT ];
	float NumberDrawer::m_digitInterval = 0.0f;
	float NumberDrawer::m_width 		= 0.0f;
	float NumberDrawer::m_height 		= 0.0f;
	bool NumberDrawer::m_initialized 	= false;

	const float NumberDrawer::DIGIT_VERTS[ DIGIT_CNT ][ MAX_EL_CNT ] =
	{
		//0
		{ 	0.0f, 0.0f, 1.0f, 0.0f,
			1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			//Padding
			0.0f, 0.0f, 0.0f, 0.0f },

		//1
		{ 	1.0f, 0.0f, 1.0f, 1.0f,
			//Padding
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f },

		//2
		{ 	1.0f, 0.0f, 0.0f, 0.0f,
		  	0.0f, 0.0f, 0.0f, 0.5f,
			0.0f, 0.5f, 1.0f, 0.5f,
			1.0f, 0.5f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f },

		//3
		{	0.0f, 0.0f, 1.0f, 0.0f,
			1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 0.5f, 1.0f, 0.5f,
			//Padding
			0.0f, 0.0f, 0.0f, 0.0f},

		//4
		{	1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 0.5f, 0.0f, 0.5f,
			0.0f, 0.5f, 0.0f, 1.0f,
			//Padding
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f },

		//5
		{ 	0.0f, 0.0f, 1.0f, 0.0f,
			1.0f, 0.0f, 1.0f, 0.5f,
			1.0f, 0.5f, 0.0f, 0.5f,
			0.0f, 0.5f, 0.0f, 1.0f,
			0.0f, 1.0f, 1.0f, 1.0f },
		//6
		{	0.0f, 0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 1.0f, 1.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			1.0f, 0.0f, 1.0f, 0.5f,
			1.0f, 0.5f, 0.0f, 0.5f },

		//7
		{	1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f,
			//Padding
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f },

		//8
		{	0.0f, 0.0f, 1.0f, 0.0f,
			1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.5f, 1.0f, 0.5f },

		//9
		{ 	0.0f, 0.0f, 1.0f, 0.0f,
			1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 0.5f,
			0.0f, 0.5f, 1.0f, 0.5f } };

	void NumberDrawer :: Initialize( float a_width, float a_height )
	{
		if ( m_initialized )
			return;

		m_width  = a_width;
		m_height = a_height;
		for ( int i = 0; i < DIGIT_CNT; ++i )
		{
			m_digitVerts[ i ] = new float [ 2 * VERTS_CNT[ i ] ];

			for ( int j = 0; j < 2 * VERTS_CNT[ i ]; j += 2 )
			{
				m_digitVerts[ i ][ j ] 		= DIGIT_VERTS[ i ][ j ] 	* m_width;
				m_digitVerts[ i ][ j + 1 ] 	= DIGIT_VERTS[ i ][ j + 1 ] * m_height;
			}
		}

		m_digitInterval = m_width / DIGIT_INTERV_FACTOR;
		m_initialized 	= true;
	}

	bool NumberDrawer :: IsInitialized()
	{
		return m_initialized;
	}

	void NumberDrawer :: DeInitilize()
	{
		if ( !m_initialized )
			return;

		for ( int i = 0; i < DIGIT_CNT; ++i )
			delete [] m_digitVerts[ i ];

		m_initialized = false;
	}

	void NumberDrawer :: DrawToRight( const Vector<float>& a_pos, uint32_t a_num )
	{
		static int digitArr[ MAX_UINT32_LEN ];
		int len = 0;

		glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );
		glEnableClientState( GL_VERTEX_ARRAY );

		do
		{
			digitArr[ len++ ] = a_num % 10;
			a_num /= 10;
		}
		while ( a_num );

		float x 		= a_pos.m_x;
		const float y	= a_pos.m_y - m_height / 2.0f;

		for ( int i = len - 1; i >=0; --i )
		{
			DrawDigit( digitArr[ i ], x, y );
			x += m_width + m_digitInterval;
		}
	}

	void NumberDrawer :: DrawToLeft( const Vector<float>& a_pos, uint32_t a_num )
	{
		float x 		= a_pos.m_x;
		const float y	= a_pos.m_y - m_height / 2.0f;

		do
		{
			int digit = a_num % 10;
			a_num	 /= 10;

			DrawDigit( digit, x, y );

			x -= m_width + m_digitInterval;
		}
		while ( a_num );
	}

	void NumberDrawer :: DrawDigit( int a_digit, float a_x, float a_y )
	{
		glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );
		glEnableClientState( GL_VERTEX_ARRAY );

		for ( int j = 0; j < 2 * VERTS_CNT[ a_digit ]; j += 2 )
		{
			m_drawer[ j ] 	  = m_digitVerts[ a_digit ][ j ] 		+ a_x;
			m_drawer[ j + 1 ] = m_digitVerts[ a_digit ][ j + 1 ] 	+ a_y;
		}

		glVertexPointer( 2, GL_FLOAT, 0, m_drawer );
		glDrawArrays( GL_LINES, 0, VERTS_CNT[ a_digit ] );
	}

}  // namespace okgame
