#include "Log.h"

#include <stdarg.h>
#include <android/log.h>

namespace okgame
{
	const char* Log::log_tags[] = { "Work", "Initialize", "Update" };

	void Log :: Error( LogTags a_tag, const char* a_msg, ... )
	{
		va_list lVarArgs;
		va_start( lVarArgs, a_msg );

		__android_log_vprint( ANDROID_LOG_ERROR, log_tags[ a_tag ], a_msg, lVarArgs );
		__android_log_print( ANDROID_LOG_ERROR, log_tags[ a_tag ], "\n" );

		va_end( lVarArgs );
	}

	void Log :: Warn( LogTags a_tag, const char* a_msg, ... )
	{
		va_list lVarArgs;
		va_start( lVarArgs, a_msg );

		__android_log_vprint( ANDROID_LOG_WARN, log_tags[ a_tag ], a_msg, lVarArgs );
		__android_log_print( ANDROID_LOG_WARN, log_tags[ a_tag ], "\n" );

		va_end( lVarArgs );
	}

	void Log :: Info( LogTags a_tag, const char* a_msg, ... )
	{
		va_list lVarArgs;
		va_start( lVarArgs, a_msg );

		__android_log_vprint( ANDROID_LOG_INFO, log_tags[ a_tag ], a_msg, lVarArgs );
		__android_log_print( ANDROID_LOG_INFO, log_tags[ a_tag ], "\n" );

		va_end( lVarArgs );
	}

	void Log :: Debug( LogTags a_tag, const char* a_msg, ... )
	{
		va_list lVarArgs;
		va_start( lVarArgs, a_msg );

		__android_log_vprint( ANDROID_LOG_DEBUG, log_tags[ a_tag ], a_msg, lVarArgs );
		__android_log_print( ANDROID_LOG_DEBUG, log_tags[ a_tag ], "\n" );

		va_end( lVarArgs );
	}
}
