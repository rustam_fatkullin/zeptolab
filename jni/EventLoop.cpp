#include "EventLoop.h"
#include "Log.h"
#include "Sensor.h"
#include "World.h"

namespace okgame
{
	EventLoop::EventLoop( android_app* pApplication )
	{
		mEnabled     				= false;
		mQuit	 	 				= false;
		mEventHandler				= NULL;
		mApplication 				= pApplication;
		mApplication->onAppCmd  	= callback_event;
		mApplication->onInputEvent	= callback_input;
		mApplication->userData 		= this;
		mSensorManager 				= NULL;
		mSensorEventQueue			= NULL;
	}

	void EventLoop::run( EventHandler& pEventHandler  )
	{
		int32_t lResult;
		int32_t lEvents;
		android_poll_source* lSource;

		app_dummy();

		mEventHandler = &pEventHandler;

		LogDebug( WorkTag, "Starting event loop" );
		while( true )
		{
			while ( ( lResult = ALooper_pollAll( mEnabled ? 0 : -1, NULL, &lEvents, (void**)&lSource ) ) >= 0 )
			{
				if ( lSource != NULL )
				{
					lSource->process( mApplication, lSource );
				}

				if ( mApplication->destroyRequested )
				{
					LogDebug( WorkTag, "Exiting event loop" );
					return;
				}
			}

			if ( mEnabled && ( !mQuit ) )
			{
				if ( mEventHandler->onStep() != RES_OK )
				{
					mQuit = true;
					ANativeActivity_finish( mApplication->activity );
				}
			}
		}
	}

	void EventLoop :: Activate()
	{
		status res = RES_OK;

		do
		{
			if ( ( !mEnabled ) && ( mApplication->window != NULL ) )
			{
				mSensorPollSource.id 		= LOOPER_ID_USER;
				mSensorPollSource.app 		= mApplication;
				mSensorPollSource.process 	= callback_sensor;
				mSensorManager 				= ASensorManager_getInstance();

				if ( mSensorManager != NULL )
				{
					mSensorEventQueue = ASensorManager_createEventQueue( mSensorManager,
																		 mApplication->looper,
																		 LOOPER_ID_USER,
																		 NULL,
																		 &mSensorPollSource );
					if ( mSensorEventQueue == NULL )
					{
						res = RES_ERROR;
						break;
					}
				}

				mEnabled = true;
				mQuit 	 = false;

				if ( mEventHandler->onActivate() != RES_OK )
				{
					res = RES_ERROR;
					break;
				}

				World* obj = ( World* )( mEventHandler );
				obj->m_accelerometer = new Sensor( *this, ASENSOR_TYPE_ACCELEROMETER );
				obj->m_accelerometer->enable();
			}
		}
		while ( 0 );

		if ( res == RES_ERROR )
		{
			mQuit = true;
			Deactivate();
			ANativeActivity_finish( mApplication->activity );

		}

		Log::Info( WorkTag, "EventLoop::activate successfully finished." );
	}

	void EventLoop :: Deactivate()
	{
		if ( mEnabled )
		{
			mEventHandler->onDeactivate();
			mEnabled = false;

			if ( mSensorEventQueue != NULL )
			{
				ASensorManager_destroyEventQueue( mSensorManager, mSensorEventQueue );
				mSensorEventQueue = NULL;
			}

			mSensorManager = NULL;
		}
	}

	void EventLoop :: ProcessActivityEvent( int32_t pCommand )
	{
		 switch (pCommand)
		 {
			case APP_CMD_CONFIG_CHANGED:
				mEventHandler->onConfigurationChanged();
				break;
			case APP_CMD_INIT_WINDOW:
				mEventHandler->onCreateWindow();
				break;
			case APP_CMD_DESTROY:
				mEventHandler->onDestroy();
				break;
			case APP_CMD_GAINED_FOCUS:
				Activate();
				mEventHandler->onGainFocus();
				break;
			case APP_CMD_LOST_FOCUS:
				mEventHandler->onLostFocus();
				Deactivate();
				break;
			case APP_CMD_LOW_MEMORY:
				mEventHandler->onLowMemory();
				break;
			case APP_CMD_PAUSE:
				mEventHandler->onPause();
				Deactivate();
				break;
			case APP_CMD_RESUME:
				mEventHandler->onResume();
				break;
			case APP_CMD_SAVE_STATE:
				mEventHandler->onSaveState( &mApplication->savedState, &mApplication->savedStateSize);
				break;
			case APP_CMD_START:
				mEventHandler->onStart();
				break;
			case APP_CMD_STOP:
				mEventHandler->onStop();
				break;
			case APP_CMD_TERM_WINDOW:
				mEventHandler->onDestroyWindow();
				Deactivate();
				break;
			default:
				break;
		}
	}

	void EventLoop :: callback_event( android_app* pApplication, int32_t pCommand )
	{
		EventLoop* eventLoop = (EventLoop*) pApplication->userData;
		eventLoop->ProcessActivityEvent( pCommand );
	}

    int32_t EventLoop :: ProcessInputEvent( AInputEvent* pEvent )
    {
        int32_t eventType = AInputEvent_getType(pEvent);
        switch ( eventType )
        {
        	case AINPUT_EVENT_TYPE_MOTION:
        		switch ( AInputEvent_getSource( pEvent ) )
        		{
				case AINPUT_SOURCE_TOUCHSCREEN:
					//Log::Info( WorkTag, "Touch!" );
					return mEventHandler->onTouchEvent( pEvent );
					break;
        		}
        		break;
        }
        return 0;
    }

    int32_t EventLoop :: callback_input( android_app* pApplication, AInputEvent* pEvent)
    {
        EventLoop* eventLoop = (EventLoop*) pApplication->userData;
        return eventLoop->ProcessInputEvent( pEvent );
    }

    void EventLoop :: processSensorEvent()
    {
    	ASensorEvent lEvent;

    	while ( ASensorEventQueue_getEvents( mSensorEventQueue, &lEvent, 1 ) > 0 )
    	{
    		switch ( lEvent.type )
    		{
    		case ASENSOR_TYPE_ACCELEROMETER :
    			mEventHandler->onAccelerometerEvent( &lEvent );
    			break;
    		}
    	}
    }

    void EventLoop :: callback_sensor( android_app* pApplication, android_poll_source* pSource )
    {
    	EventLoop& lEventLoop = *( EventLoop* )pApplication->userData;
    	lEventLoop.processSensorEvent();
    }
}
