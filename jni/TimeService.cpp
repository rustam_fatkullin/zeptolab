#include "TimeService.h"
#include "Types.h"
#include "Log.h"

namespace okgame
{
	TimeService :: TimeService()
	{
		m_elapsed  = 0.0f;
		m_lastTime = 0.0;

		Log::Info( okgame::InitializeTag, "TimeService is initialized." );
	}

	void TimeService :: Reset()
	{
		m_elapsed  = 0.0f;
		m_lastTime = Now();
	}

	double TimeService :: Now()
	{
		timespec timeVal;
		clock_gettime( CLOCK_MONOTONIC, &timeVal );

		return timeVal.tv_sec + ( timeVal.tv_nsec * 1.0e-9 );
	}

	float TimeService :: Elapsed()
	{
		double currentTime = Now();

		m_elapsed  = currentTime - m_lastTime;
		m_lastTime = currentTime;

		return m_elapsed;
	}

} // namespace okgame




