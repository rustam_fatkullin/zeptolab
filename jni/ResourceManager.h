#ifndef _OKGAME_RESOURCE_MANAGER_H_
#define _OKGAME_RESOURCE_MANAGER_H_

#include <set>
#include <map>

#include "IdKeeper.h"
#include "Asteroid.h"
#include "Bullet.h"

namespace okgame
{
	class ResourceManager
	{
	public :
		ResourceManager( int a_maxAstCnt, int a_maxBulletCnt );
		~ResourceManager();

		bool GetMemForAsteroid( int& a_id, GameObject*& a_newAst );
		bool GetMemForBullet( int& a_id, GameObject*& a_newBullet );

		void PutBackAsteroidMem( int a_id );
		void PutBackBulletMem( int a_id );

	private :

		bool GetMem( int& a_id, GameObject*& a_mem, std::set<int>& a_unusedId, std::map<int, GameObject*>& a_memMap );
		void PutMem( int a_id, std::set<int>& a_unusedIds, std::map<int, GameObject*>& a_memMap );

		std::set<int> 				m_unusedAstIds;
		std::map<int, GameObject*>	m_asteroidsMem;

		std::set<int> 				m_unusedBulletIds;
		std::map<int, GameObject*>	m_bulletsMem;

		IdKeeper					m_idKeeper;
	};

} //namespace okgame

#endif
