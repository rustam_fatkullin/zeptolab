#include <GLES/gl.h>
#include <GLES/glext.h>
#include <stdlib.h>

#include "Figure.h"

namespace okgame
{
	Figure :: Figure( FigType a_type, const Vector<float>& a_pos, int a_vertsCnt, const float* a_verts )
		: m_type( a_type ),
		  m_pos( a_pos ),
		  m_size( 2 * a_vertsCnt ),
		  m_vertsCnt( a_vertsCnt )
	{
		m_verts = new float [ 2 * m_vertsCnt ];
		memcpy( m_verts, a_verts, 2 * a_vertsCnt * sizeof( float ) );
	}


	Figure :: ~Figure()
	{
		delete [] m_verts;
	}

	void Figure :: Update( const Vector<float>& a_pos, int a_vertsCnt, const float* a_verts )
	{
		if ( m_size < 2 * a_vertsCnt )
			return;

		m_vertsCnt = a_vertsCnt;
		m_pos = a_pos;

		for ( int i = 0; i < 2 * m_vertsCnt; i += 2 )
		{
			m_verts[ i ] 		= a_verts[ i ] 	   + a_pos.m_x;
			m_verts[ i + 1 ] 	= a_verts[ i + 1 ] + a_pos.m_y;
		}
	}

	void Figure :: RotateAtPoint( const Vector<float>& a_pos, float a_angle )
	{
		m_pos.RotateAtPoint( a_pos, a_angle );

		for ( int i = 0; i < 2 * m_vertsCnt; i += 2 )
			Geometry::RotateAtPoint( a_pos.m_x, a_pos.m_y, a_angle, m_verts[ i ], m_verts[ i + 1 ] );
	}

	Vector<float> Figure :: GetPos() const
	{
		return m_pos;
	}

	void Figure :: SetPos( const Vector<float>& a_newPos )
	{
		const Vector<float> shiftVec = a_newPos - m_pos;
		m_pos = a_newPos;

		for ( int i = 0; i < 2 * m_vertsCnt; i += 2 )
		{
			m_verts[ i ] 		= m_verts[ i ] 	   + shiftVec.m_x;
			m_verts[ i + 1 ] 	= m_verts[ i + 1 ] + shiftVec.m_y;
		}
	}

	int Figure :: GetVertsCnt() const
	{
		return m_vertsCnt;
	}

	const float* Figure :: GetVerts() const
	{
		return m_verts;
	}

	void Figure :: Shift( const Vector<float>& a_shift )
	{
		m_pos = m_pos + a_shift;

		for ( int i = 0; i < 2 * m_vertsCnt; i += 2 )
		{
			m_verts[ i ] 	 += a_shift.m_x;
			m_verts[ i + 1 ] += a_shift.m_y;
		}
	}

	void Figure :: Draw() const
	{
		glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );
		glEnableClientState( GL_VERTEX_ARRAY );
		glVertexPointer( 2, GL_FLOAT, 0, m_verts );
		glDrawArrays( GL_LINE_LOOP, 0, m_vertsCnt );
	}


} //namespace okgame
