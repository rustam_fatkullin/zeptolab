#ifndef _OKGAME_GAME_OBJECT_H_
#define _OKGAME_GAME_OBJECT_H_

#include "Vector.h"
#include "Figure.h"

namespace okgame
{
	class GameObject
	{
	public :

		enum ObjectType { USER_SHIP, BULLET, ASTEROID, FRAG_ASTEROID };

		GameObject( ObjectType a_type, const Vector<float>& a_vel, float a_rad  );
		virtual ~GameObject();

		ObjectType GetType();
		void Draw();
		Vector<float> GetPos() const;
		Vector<float> GetVel() const;
		float GetRad() const;
		void Shift( const Vector<float>& a_shift );
		void RotateAtPoint( const Vector<float>& a_pos, float a_angle );
		virtual void Update( float a_elapsedTime );

		const Figure* GetFigure() const;

		virtual bool CollideWithShip( const GameObject* a_obj ) const = 0;
		virtual bool CollideWithBullet( const  GameObject* a_obj ) const = 0;
		virtual bool CollideWithAsteroid( const GameObject* a_obj  ) const = 0;

	protected :

		ObjectType		m_type;
		Figure*			m_figure;
		Vector<float>	m_vel;
		float			m_rad;
	};

} //namespace okgame

#endif
