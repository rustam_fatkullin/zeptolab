#ifndef _OKGAME_EVENTLOOP_HPP_
#define _OKGAME_EVENTLOOP_HPP_

#include <android_native_app_glue.h>
#include "EventHandler.h"
#include "Types.h"

namespace okgame
{
	class EventLoop
	{
	public :
		EventLoop( android_app* pApplication );
		void run( EventHandler& pEventHandler );

	protected :
		void Activate();
		void Deactivate();
		void ProcessActivityEvent( int32_t pCommand );
		int32_t ProcessInputEvent(AInputEvent* pEvent);
		void processSensorEvent();

	private :

		friend class Sensor;

		static void callback_event( android_app* pApplication, int32_t pCommand );
	    static int32_t callback_input(android_app* pApplication, AInputEvent* pEvent);
	    static void callback_sensor( android_app* pApplication, android_poll_source* pSource );

		bool			mEnabled;
		bool			mQuit;
		EventHandler*	mEventHandler;
		android_app* 	mApplication;

		ASensorManager* 	mSensorManager;
		ASensorEventQueue*	mSensorEventQueue;
		android_poll_source mSensorPollSource;
	};
}

#endif
