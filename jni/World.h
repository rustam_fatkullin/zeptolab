#ifndef _OKGAME_WORLD_H_
#define _OKGAME_WORLD_H_

#include <set>
#include <list>
#include <map>
#include <stddef.h>

#include "Vector.h"
#include "EventHandler.h"
#include "Asteroid.h"
#include "Ship.h"
#include "Sensor.h"
#include "Circle.h"
#include "ResourceManager.h"

namespace okgame
{
	class World : public EventHandler
	{
	public :

		static const float RESIST_FAC = 0.5;

		World();
		~World();

		//Event handling methods
		void OnStep();

		status onActivate();
		void   onDeactivate();
		status onStep();
		void   onStart();
		void onResume();
		void onPause();
		void onStop();
		void onDestroy();

		void onSaveState(void** pData, size_t* pSize);
		void onConfigurationChanged();
		void onLowMemory();

		void onCreateWindow();
		void onDestroyWindow();
		void onGainFocus();
		void onLostFocus();

		//Input event handling methods

		bool onTouchEvent( AInputEvent* pEvent );
		bool onAccelerometerEvent( ASensorEvent* pEvent );

		Sensor* m_accelerometer;

	private :

		enum GameState { MENU, DEPLOYING, PLAYING };

		//Ship
		static const int SHIP_START_VEL_FACTOR 	= 3;
		static const int SHIP_WIDTH_FACTOR 	 	= 30;
		static const int SHIP_HEIGHT_FACTOR	 	= SHIP_WIDTH_FACTOR * 0.5f;
		static const int BULLET_VEL_FACTOR	 	= 2;
		static const int MAX_BULLET_CNT			= 100;
		static const Vector<float> BULLET_INIT_DIR;

		//Interface
		static const int INTERF_FACTOR			= 1;
		static const int INIT_LIFE_CNT			= 3;

		//Asteroid
		static const int AST_MIN_RAD_FACTOR		= 150;
		static const int AST_MAX_RAD_FACTOR		= 40;
		static const int AST_FRAG_CNT 			= 3;
		static const int AST_CNT 				= 20;
		static const int MAX_AST_CNT			= AST_FRAG_CNT * AST_CNT;
		static const int AST_ATTACK_RAD_FACTOR 	= 3;

		static const int MENU_MAX_RAD_FAC 		= 5;
		static const int MENU_MIN_RAD_FAC 		= 15;
		static const int MENU_DEF_RAD			= 10;
		static const float MENU_DECREASE_FAC;
		static const float MENU_POS_FACTOR;
		static const float MENU_CHANGE_INTERVAL;

		static const float DEPLOY_TIME_INTERVAL;
		static const float FLICKER_TIME_INTERVAL;

		void SessionStart();
		void NextState();
		void Draw( float elpasedTime );
		void MoveObjects( float a_elapsedTime );
		void NextState( float a_elapsedTime );
		void RotateWorld( float a_side );
		void DrawInterface();

		void CollisionDetect();
		void CheckOutObjects();
		bool CheckShipCoollision();

		//Menu
		void MenuInitialize();
		void MenuUpdate( float a_elapsedTime );
		void MenuDraw();

		//Asteroids
		void AddAsteroid( const Vector<float>& a_pos, const Vector<float>& a_dir, bool a_fragment );
		void DeleteAsteroid( int a_id );
		void GenerateAsteroids();
		void GenerateFragmentedAsteroids( const std::list<Vector<float> >& a_toAdd );

		//Bullets
		void AddBullet();
		void DeleteBullet( int a_id );

		void 						Initialize();
		void 						DeInitialize();

		GameState					m_state;
		bool						m_initialized;

		Vector<float>				m_displaySize;

		Ship* 						m_ship;
		Vector<float>				m_shipTop;
		Vector<float>				m_bulletVel;


		int							m_astCnt;
		int							m_bulletCnt;

		std::map<int, GameObject*>	m_worldObjects;

		float						m_deployTime;
		float						m_flickerTime;
		bool						m_visibleMode;


		ResourceManager*			m_resManager;

		//Interface
		Figure*						m_lifeIndicFigure;
		Vector<float>				m_lifeIndicPos;
		Vector<float>				m_lifeIndicInterVec;
		Vector<float>				m_scoreIndicPos;
		float						m_sessionTime;

		int 						m_life;

		float						m_destroyRadius;
		float 						m_astCreateRadius;
		float 						m_astAttackRadius;

		//Menu
		Circle* 					m_menuCircle;
		float						m_maxMenuRad;
		float 						m_minMenuRad;
		float						m_menuChangeTimer;
	};

} //namespace okgame

#endif
