#ifndef _OKGAME_VECTOR_H_
#define _OKGAME_VECTOR_H_

#include <math.h>

#include "Geometry.h"

namespace okgame
{
	template<typename T>
	class Vector
	{
	public :
		Vector();
		Vector( T a_x, T a_y );

		void Rotate( T a_angle );
		void RotateAtPoint( const Vector<T>& a_vec, T a_angle );
		void ToUnit();
		T DistTo( const Vector<T>& a_b ) const;

		T m_x;
		T m_y;

		template<typename L>
		friend Vector<L> operator + ( const Vector<L>& a_vecA, const Vector<L>& a_vecB );

		template<typename L>
		friend Vector<L> operator - ( const Vector<L>& a_vecA, const Vector<L>& a_vecB );

		template<typename L>
		friend Vector<L> operator * ( float a_val, const Vector<L>& a_vecB );

		template<typename L>
		friend Vector<L> operator * ( const Vector<L>& a_vecB, float a_val );

		template<typename L>
		friend Vector<L> operator * ( int a_val, const Vector<L>& a_vecB );

		template<typename L>
		friend Vector<L> operator * ( const Vector<L>& a_vecB, int a_val );
	};

	template<typename T>
	Vector<T> :: Vector()
		:
		m_x( 0 ),
		m_y( 0 )
	{}

	template<typename T>
	Vector<T> :: Vector( T a_x, T a_y )
		:
		m_x( a_x ),
		m_y( a_y )
	{}

	template<typename T>
	Vector<T> operator + ( const Vector<T>& a_vecA, const Vector<T>& a_vecB )
	{
		return Vector<T>( a_vecA.m_x + a_vecB.m_x, a_vecA.m_y + a_vecB.m_y );
	}

	template<typename T>
	Vector<T> operator - ( const Vector<T>& a_vecA, const Vector<T>& a_vecB )
	{
		return Vector<T>( a_vecA.m_x - a_vecB.m_x, a_vecA.m_y - a_vecB.m_y );
	}

	template<typename T>
	Vector<T> operator * ( float a_val, const Vector<T>& a_vec )
	{
		return Vector<T>( a_val * a_vec.m_x, a_val * a_vec.m_y );
	}

	template<typename T>
	Vector<T> operator * ( const Vector<T>& a_vec, float a_val )
	{
		return Vector<T>( a_val * a_vec.m_x, a_val * a_vec.m_y );
	}

	template<typename T>
	Vector<T> operator * ( int a_val, const Vector<T>& a_vec )
	{
		return Vector<T>( a_val * a_vec.m_x, a_val * a_vec.m_y );
	}

	template<typename T>
	Vector<T> operator * ( const Vector<T>& a_vec, int a_val )
	{
		return Vector<T>( a_val * a_vec.m_x, a_val * a_vec.m_y );
	}

	template<typename T>
	void Vector<T> :: Rotate( T a_angle )
	{
		T cosVal = cos( a_angle );
		T sinVal = sin( a_angle );

		T x = m_x;
		T y = m_y;

		m_x =  cosVal * x + sinVal * y;
		m_y = -sinVal * x + cosVal * y;
	}

	template<typename T>
	void Vector<T> :: RotateAtPoint( const Vector<T>& a_p, T a_angle )
	{
		*this = *this - a_p;

		this->Rotate( a_angle );

		*this = *this + a_p;
	}

	template<typename T>
	T Vector<T> :: DistTo( const Vector<T>& a_b ) const
	{
		return sqrt( ( a_b.m_x - m_x ) * ( a_b.m_x - m_x ) + ( a_b.m_y - m_y ) * ( a_b.m_y - m_y ) );
	}

	template<typename T>
	void Vector<T> :: ToUnit()
	{
		T len = sqrt( m_x * m_x + m_y * m_y );

		if ( len < Geometry::EPS )
			return;

		m_x /= len;
		m_y /= len;
	}

} //namespace okgame

#endif
