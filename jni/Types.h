#ifndef _OKGAME_TYPES_H_
#define _OKGAME_TYPES_H_

#include <stdint.h>
#include <stddef.h>

namespace okgame
{
	typedef int32_t status;

	const status RES_OK 	=  0;
	const status RES_ERROR	= -1;
	const status RES_EXIT 	= -2;

} //namespace okgame

#endif
