#ifndef _OKGAME_BULLET_H_
#define _OKGAME_BULLET_H_

#include "Vector.h"
#include "GameObject.h"

namespace okgame
{
	class Bullet : public GameObject
	{
	public :

		static void Initialize();

		Bullet();
		Bullet( const Vector<float>& a_pos, const Vector<float>& a_vel );
		~Bullet();

		void Construct( const Vector<float>& a_pos, const Vector<float>& a_vel );

		virtual float GetRad() const;
		virtual bool CollideWithShip( const GameObject* a_obj ) const;
		virtual bool CollideWithBullet( const GameObject* a_obj ) const;
		virtual bool CollideWithAsteroid( const GameObject* a_obj ) const;

	private :

		static const int 	VERTS_CNT = 4;
		static const int 	EL_CNT    = 2 * VERTS_CNT;
		static const float 	HALF_WIDTH;
		static const float 	m_constrcutVerts[ EL_CNT ];
	};

} //namespace okgame

#endif
