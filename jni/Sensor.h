#ifndef _OKGAME_SENSOR_H_
#define _OKGAME_SENSOR_H_

#include <android/sensor.h>

#include "Types.h"

namespace okgame
{
	class EventLoop;

	class Sensor
	{
	public :
		Sensor( EventLoop& pEventLoop, int32_t pSensorType );

		status toggle();
		status enable();
		status disable();

	private :
		EventLoop& 		mEventLoop;
		const ASensor* 	mSensor;
		int32_t 		mSensorType;
	};
} // namespace okgame

#endif
