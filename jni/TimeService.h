#ifndef _OKGAME_TIME_SERVICE_HPP_
#define _OKGAME_TIME_SERVICE_HPP_

#include <time.h>
#include "Types.h"

namespace okgame
{
	class TimeService
	{
	public :
		TimeService();

		double Now();
		float  Elapsed();
		void   Reset();

	private :
		float  m_elapsed;
		double m_lastTime;
	};
}

#endif
