#ifndef _OKGAME_GEOMETRY_H_
#define _OKGAME_GEOMETRY_H_

#include <utility>

namespace okgame
{
	class Geometry
	{
	public :
		static const float 	EPS;
		static const float 	PI;

		static void  RotateAtPoint( float a_pX, 	float a_pY,
									float a_angle,
									float& a_vX, 	float& a_vY );

		static float Dist( 	float a_ax, 	float a_ay,
							float a_bx, 	float a_by );

		static bool  InPolygon( float a_x, 	float a_y,
								int a_n, 	float *a_verts );

		static float PseudoScalarMul( 	float a_aX, float a_aY,
										float a_bX, float a_bY );

		static bool  PointInSegment( 	float a_x, 			float a_y,
										float a_segStartX, 	float a_segStartY,
										float a_segEndX, 	float a_segEndY );

		static std::pair<float, float>  RayPolygonIntersect( 	float a_startX,     float a_startY,
															   	float a_dirVecX,    float a_dirVecY,
															   	float a_segStartX,  float a_segStartY,
															   	float a_segEndX,    float a_segEndY );

		static bool PolygonsIntersection( 	int a_aVertsCnt, const float* a_aVerts,
											int a_bVertsCnt, const float* a_bVerts );
		static float GetRandomAngle();

		static bool InInterval( float a_begin, float a_end, float a_target );
		static bool InSegment( float a_begin, float a_end, float a_target );
	};

} //namespace okgame

#endif
