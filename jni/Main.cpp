#include "EventLoop.h"
#include "EventHandler.h"
#include "Context.h"
#include "World.h"
#include "Log.h"
#include "TimeService.h"
#include "GraphicService.h"
#include "Sensor.h"

void android_main( android_app* a_app )
{
	{
		using namespace okgame;

		Context::m_app 				= a_app;
		Context::m_timeService 		= new TimeService();
		Context::m_graphicsService 	= new GraphicsService();

		World world;

		okgame::EventLoop eventLoop( a_app  );

		eventLoop.run( world );

		Asteroid :: DeInitialize();
	}
}
