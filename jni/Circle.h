#ifndef _OKGAME_CIRCLE_H_
#define _OKGAME_CIRCLE_H_

#include "Figure.h"
#include "Vector.h"

namespace okgame
{
	class Circle : public Figure
	{
	public :

		Circle( const Vector<float>& a_pos, float a_rad );
		~Circle();

		float GetRad();
		void SetRad( float a_rad );

		virtual bool IntersectWithPoint( const Vector<float>& a_pos ) const;
		virtual bool IntersectWithPolygon( const Figure* a_figure ) const;
		virtual bool IntersectWithCircle( const Vector<float>& a_pos, float a_rad ) const;

	private :

		static const int VERTS_CNT = 50;

		void ConstructPoints();

		float m_rad;

	};

} //namespace okgame

#endif
