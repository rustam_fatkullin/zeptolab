#include <GLES/gl.h>
#include <GLES/glext.h>
#include <stdlib.h>

#include "Bullet.h"
#include "Geometry.h"
#include "Polygon.h"

namespace okgame
{
	const float Bullet::HALF_WIDTH = 0.5f;
	const float Bullet::m_constrcutVerts[ EL_CNT ] = { 	 HALF_WIDTH, -HALF_WIDTH,
														 HALF_WIDTH,  HALF_WIDTH,
														-HALF_WIDTH,  HALF_WIDTH,
														-HALF_WIDTH, -HALF_WIDTH };

	Bullet :: Bullet()
		: GameObject( GameObject::BULLET, Vector<float>( 0.0f, 0.0f ), HALF_WIDTH )
	{
		m_figure = new Polygon( Vector<float>( 0.0f, 0.0f ), VERTS_CNT, m_constrcutVerts );
	}

	Bullet :: Bullet( const Vector<float>& a_pos, const Vector<float>& a_vel )
		: GameObject( GameObject::BULLET, a_vel, HALF_WIDTH )
	{
		m_rad    = HALF_WIDTH;
		m_figure = new Polygon( a_pos, VERTS_CNT, m_constrcutVerts );
	}

	void Bullet :: Construct( const Vector<float>& a_pos, const Vector<float>& a_vel )
	{
		m_vel = a_vel;
		m_figure->Update( a_pos, VERTS_CNT, m_constrcutVerts );
	}

	Bullet :: ~Bullet()
	{
		delete m_figure;
	}

	float Bullet :: GetRad() const
	{
		return HALF_WIDTH;
	}

	bool Bullet :: CollideWithShip( const GameObject* a_obj ) const
	{
		return a_obj->GetFigure()->IntersectWithPoint( m_figure->GetPos() );
	}

	bool Bullet :: CollideWithBullet( const GameObject* a_obj ) const
	{
		return m_figure->GetPos().DistTo( a_obj->GetFigure()->GetPos() ) < Geometry::EPS;
	}

	bool Bullet :: CollideWithAsteroid( const GameObject* a_obj ) const
	{
		return a_obj->GetFigure()->IntersectWithPoint( m_figure->GetPos() );
	}

} //namespace okgame

