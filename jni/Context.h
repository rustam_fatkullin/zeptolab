#ifndef _OKGAME_CONTEXT_HPP_
#define _OKGAME_CONTEXT_HPP_

#include <android_native_app_glue.h>
#include "Types.h"

namespace okgame
{
	class TimeService;
	class GraphicsService;
	class GraphicsTexture;

	class Context
	{
	public :
		static TimeService* 		m_timeService;
		static GraphicsService* 	m_graphicsService;
		static android_app*			m_app;
	};
}

#endif
