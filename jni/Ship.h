#ifndef _OKGAME_SHIP_H_
#define _OKGAME_SHIP_H_

#include <vector>
#include "Vector.h"
#include "Bullet.h"
#include "Figure.h"
#include "GameObject.h"

namespace okgame
{
	class Ship : public GameObject
	{
	public :

		 Ship( const Vector<float>& a_pos, float a_startVel, float a_width, float a_height );
		~Ship();

		Vector<float> GetTop();
		void Update( float a_elapsedTime );
		void SetStartVelocity();

		static Vector<float> CreateShipFigure( Figure*& a_figure, const Vector<float>& a_pos, float a_width, float a_height );

		virtual bool CollideWithShip( const GameObject* a_obj ) const;
		virtual bool CollideWithBullet( const  GameObject* a_obj ) const;
		virtual bool CollideWithAsteroid( const GameObject* a_obj  ) const;

	private :

		static const Vector<float> 	FIRE_DIR;
		static const int 			VERTS_CNT = 4;
		static const int 			EL_CNT    = 2 * VERTS_CNT;

		Vector<float>	m_startVelocity;
		Vector<float> 	m_top;
	};

} //namespace okgame

#endif
